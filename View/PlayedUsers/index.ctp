<?php 
echo $this->Html->css('https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css');

// echo $this->Html->script('https://code.jquery.com/jquery-1.12.4.js');
echo $this->Html->script('https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js');
echo $this->Html->script('https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js');

?>

<div class="playedUsers index">
	<h2><?php echo __('Played Records'); ?></h2>
	<table id="records" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th data-orderable="false">ID</th>
				<th>Name</th>
				<th><?php echo $this->Paginator->sort('message'); ?></th>
				<th>Created</th>
				<th class="actions"><?php echo __('Actions'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($playedUsers as $playedUser): ?>
				<tr>
					<td><?php echo h($playedUser['PlayedUser']['id']); ?>&nbsp;</td>
					<td><?php echo h($playedUser['PlayedUser']['name']); ?>&nbsp;</td>
					<td><?php echo h($playedUser['PlayedUser']['message']); ?>&nbsp;</td>
					<td><?php echo h($playedUser['PlayedUser']['created']); ?>&nbsp;</td>
					<td class="actions">
						<div class="btn-group">
							<?php echo $this->Html->link(__('View'), array('action' => 'view', $playedUser['PlayedUser']['id']),array('class'=>'btn btn-success')); ?>
							<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $playedUser['PlayedUser']['id']),array('class'=>'btn btn-success')); ?>
							<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $playedUser['PlayedUser']['id']), array('class'=>'btn btn-danger','confirm' => __('Are you sure you want to delete # %s?', $playedUser['PlayedUser']['id']))); ?>
						</div>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>

<script>
	$(document).ready(function() {
		$('#records').DataTable({
			"columns":[
			null,
			{"width":"20%"},
			null,
			null,
			null
			]
		});

	} );

</script>