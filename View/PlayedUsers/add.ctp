<div class="row">
	<div class="col-sm-6 col-sm-offset-3 col-md-4 col-lg-12 col-lg-offset-0">
		<div class="panel panel-default fc-panel">
			<div class="panel-heading">
				<?php echo $this->Html->image('Ribena-Logo.png',array('class'=>'ribena-logo img-responsive'));?>
				<?php echo $this->Html->image('greet_font.png',array('class'=>'greet img-responsive'));?>
			</div>
			<div class="panel-body">
				<?php echo $this->Form->create('PlayedUser',array('name'=>'grtForm','id'=>'grtForm','class'=>'form-horizontal')); ?>
				<div class="form-group" id="nameInput">
					<label for="name" class="col-sm-2 control-label text-center" style="padding-top: 0px !important;"><h4>FROM</h4></label>
					<div class="col-sm-7 col-lg-3">
						<?php echo $this->Form->input('name',array('class'=>'form-control name','id'=>'name','label'=>false,'div'=>false,'maxlength'=>35));?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<h4 class="text-center">Please Select A Message</h4>
						<div class="row form-group msg-chooser">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
								<div class="msg-chooser-item selected text-center">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<?php echo $this->Html->image('ribena/CNY_Greeting-01.png',array('class'=>'img-responsive'));?>
										<input class="message" type="radio" name="data[PlayedUser][message]" value="msg1" checked="checked">
									</div>
									<div class="clear"></div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
								<div class="msg-chooser-item">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<?php echo $this->Html->image('ribena/CNY_Greeting-02.png',array('class'=>'img-responsive'));?>
										<input class="message" type="radio" name="data[PlayedUser][message]" value="msg2">
									</div>
									<div class="clear"></div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
								<div class="msg-chooser-item">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<?php echo $this->Html->image('ribena/CNY_Greeting-03.png',array('class'=>'img-responsive'));?>
										<input class="message" type="radio" name="data[PlayedUser][message]" value="msg3">
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<?php echo $this->Form->end();?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		$('div.msg-chooser').not('.disabled').find('div.msg-chooser-item').on('click', function(){
			$(this).parent().parent().find('div.msg-chooser-item').removeClass('selected');
			$(this).addClass('selected');
			$(this).find('input[type="radio"]').prop("checked", true);
		});

		$('.msg-chooser-item').on('click tap', function(event) {
			event.preventDefault();
			if($.trim($('#name').val()) == ''){
				alert('Please enter your name');
			}else{
				$('#grtForm').submit();
			}
			
		});

		$('input.name').maxlength({
			threshold: 20
		});
	});
</script>