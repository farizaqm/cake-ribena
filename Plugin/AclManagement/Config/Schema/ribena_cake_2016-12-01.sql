# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.9-MariaDB)
# Database: ribena_cake
# Generation Time: 2016-12-01 11:24:51 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`)
VALUES
	(1,NULL,NULL,NULL,'controllers',1,80),
	(2,1,NULL,NULL,'Pages',2,5),
	(3,2,NULL,NULL,'display',3,4),
	(4,1,NULL,NULL,'AclManagement',6,59),
	(5,4,NULL,NULL,'Groups',7,18),
	(6,5,NULL,NULL,'index',8,9),
	(7,5,NULL,NULL,'view',10,11),
	(8,5,NULL,NULL,'add',12,13),
	(9,5,NULL,NULL,'edit',14,15),
	(10,5,NULL,NULL,'delete',16,17),
	(11,4,NULL,NULL,'UserPermissions',19,28),
	(12,11,NULL,NULL,'index',20,21),
	(13,11,NULL,NULL,'sync',22,23),
	(14,11,NULL,NULL,'edit',24,25),
	(15,11,NULL,NULL,'toggle',26,27),
	(16,4,NULL,NULL,'Users',29,58),
	(17,16,NULL,NULL,'login',30,31),
	(18,16,NULL,NULL,'logout',32,33),
	(19,16,NULL,NULL,'index',34,35),
	(20,16,NULL,NULL,'view',36,37),
	(21,16,NULL,NULL,'add',38,39),
	(22,16,NULL,NULL,'edit',40,41),
	(23,16,NULL,NULL,'delete',42,43),
	(24,16,NULL,NULL,'toggle',44,45),
	(25,16,NULL,NULL,'register',46,47),
	(26,16,NULL,NULL,'confirm_register',48,49),
	(27,16,NULL,NULL,'forgot_password',50,51),
	(28,16,NULL,NULL,'activate_password',52,53),
	(29,16,NULL,NULL,'edit_profile',54,55),
	(30,16,NULL,NULL,'confirm_email_update',56,57),
	(31,1,NULL,NULL,'PlayedUsers',60,79),
	(32,31,NULL,NULL,'index',61,62),
	(33,31,NULL,NULL,'view',63,64),
	(34,31,NULL,NULL,'add',65,66),
	(35,31,NULL,NULL,'edit',67,68),
	(36,31,NULL,NULL,'delete',69,70),
	(37,31,NULL,NULL,'playAnim',71,72),
	(38,31,NULL,NULL,'getPlayData',73,74),
	(39,31,NULL,NULL,'preview',75,76),
	(40,31,NULL,NULL,'share',77,78);

/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`)
VALUES
	(1,NULL,'Group',1,NULL,1,4),
	(2,1,'User',1,NULL,2,3);

/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros_acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`)
VALUES
	(1,1,1,'1','1','1','1');

/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `created`, `modified`)
VALUES
	(1,'admin','2016-11-30 06:14:50','2016-11-30 06:14:50');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table played_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `played_users`;

CREATE TABLE `played_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `message` text,
  `fileLink` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `played_users` WRITE;
/*!40000 ALTER TABLE `played_users` DISABLE KEYS */;

INSERT INTO `played_users` (`id`, `name`, `message`, `fileLink`, `created`, `modified`)
VALUES
	(1,'FARIZ','msg1','2016-12-01/img_20161201100246.png','2016-12-01 10:02:48','2016-12-01 10:02:48'),
	(2,'Fariz','msg3','2016-12-01/img_20161201101940.png','2016-12-01 10:19:42','2016-12-01 10:19:42'),
	(3,'FARIZ','msg1','2016-12-01/img_20161201105430.png','2016-12-01 10:54:33','2016-12-01 10:54:33'),
	(4,'FARIZ','msg2','2016-12-01/img_20161201105609.png','2016-12-01 10:56:11','2016-12-01 10:56:11'),
	(5,'FARIZ','msg2','2016-12-01/img_20161201105654.png','2016-12-01 10:56:56','2016-12-01 10:56:56'),
	(6,'FARIZ','msg2','2016-12-01/img_20161201105730.png','2016-12-01 10:57:33','2016-12-01 10:57:33'),
	(7,'FARIZ','msg2','2016-12-01/img_20161201105804.png','2016-12-01 10:58:07','2016-12-01 10:58:07'),
	(8,'FARIZ','msg2','2016-12-01/img_20161201105845.png','2016-12-01 10:58:47','2016-12-01 10:58:47'),
	(9,'FARIZ','msg1','2016-12-01/img_20161201105938.png','2016-12-01 10:59:40','2016-12-01 10:59:40'),
	(10,'FARIZ','msg1','2016-12-01/img_20161201105957.png','2016-12-01 10:59:59','2016-12-01 10:59:59'),
	(11,'FARIZ','msg1','2016-12-01/img_20161201110018.png','2016-12-01 11:00:20','2016-12-01 11:00:20'),
	(12,'FARIZ','msg1','2016-12-01/img_20161201110025.png','2016-12-01 11:00:27','2016-12-01 11:00:27'),
	(13,'FARIZ','msg1','2016-12-01/img_20161201110103.png','2016-12-01 11:01:05','2016-12-01 11:01:05'),
	(14,'FARIZ','msg1','2016-12-01/img_20161201110159.png','2016-12-01 11:02:02','2016-12-01 11:02:02'),
	(15,'FARIZ','msg1','2016-12-01/img_20161201110230.png','2016-12-01 11:02:32','2016-12-01 11:02:32'),
	(16,'FARIZ','msg1','2016-12-01/img_20161201110258.png','2016-12-01 11:03:00','2016-12-01 11:03:00'),
	(17,'FARIZ','msg1','2016-12-01/img_20161201110312.png','2016-12-01 11:03:14','2016-12-01 11:03:14'),
	(18,'FARIZ','msg1','2016-12-01/img_20161201110343.png','2016-12-01 11:03:45','2016-12-01 11:03:45'),
	(19,'FARIZ','msg1','2016-12-01/img_20161201110433.png','2016-12-01 11:04:35','2016-12-01 11:04:35'),
	(20,'FARIZ','msg1','2016-12-01/img_20161201110455.png','2016-12-01 11:04:58','2016-12-01 11:04:58'),
	(21,'FARIZ IZWAN','msg1','2016-12-01/img_20161201110656.png','2016-12-01 11:06:59','2016-12-01 11:06:59'),
	(22,'far','msg1','2016-12-01/img_20161201120544.png','2016-12-01 12:05:47','2016-12-01 12:05:47'),
	(23,'Fariz Izwan & Family','msg1','2016-12-01/img_20161201121311.png','2016-12-01 12:13:13','2016-12-01 12:13:13');

/*!40000 ALTER TABLE `played_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` tinytext COLLATE utf8_unicode_ci COMMENT 'full url to avatar image file',
  `language` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `group_id`, `username`, `name`, `password`, `email`, `avatar`, `language`, `timezone`, `token`, `status`, `created`, `modified`, `last_login`)
VALUES
	(1,1,NULL,'admin','0ccc35f8be81c64a46b6b50dc4e88b24c09c03ee','admin@aqm.com.my',NULL,NULL,NULL,NULL,1,'2016-11-30 06:15:13','2016-11-30 06:15:13','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
