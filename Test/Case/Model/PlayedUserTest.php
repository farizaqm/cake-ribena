<?php
App::uses('PlayedUser', 'Model');

/**
 * PlayedUser Test Case
 */
class PlayedUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.played_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PlayedUser = ClassRegistry::init('PlayedUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PlayedUser);

		parent::tearDown();
	}

}
